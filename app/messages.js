const express = require('express');
const nanoid = require('nanoid');
const db = require('../fileDb');
const multer = require ('multer');
const path = require('path');
const config = require ('../config');


const storage = multer.diskStorage({
    destination:  (req, file, cb)=> {
        cb(null, config.uploadPath);
    },
    filename:  (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const router = express.Router();


const upload = multer({storage});


router.get('/', (req, res) => {
        res.send(db.getItems())
});

router.post('/', upload.single('image') , (req, res) => {
    const message = req.body;

    if(req.file) {
        message.image = req.file.filename;
    }

    if(req.body.message !== '') {
        if(req.body.author === '') {
            message.author = 'Anonymous'
        }

        message.date = new Date().toISOString();
        message.id = nanoid();
        db.addItem(message);
        res.send({message: 'OK'})
    } else {
        res.status(500).send({error: 'Message mustbe present in the request!'})
    }
});



module.exports = router;

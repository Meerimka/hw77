const fs = require('fs');

const filename = './db.json';

let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);

        data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }
  },
  getItems() {
    this.init();
      return data.splice(data.length-30);
  },
  addItem(item) {
    this.init();
    data.push(item);
      this.save();
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(data, null, 2));
  },
    getByDate(date) {
    this.init();
        return data.filter(message => {
          return message.date > date
        })
    }
};
